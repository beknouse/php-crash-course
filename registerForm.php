<?php
    require_once 'meekrodb.2.3.class.php';
    DB::$user = 'root';
    DB::$dbName = 'MicroBlog';

    $username = $_POST['username'];
    $password = $_POST['password'];
    $hash = password_hash($password, PASSWORD_DEFAULT);

    DB::insert('credentials', array(
        'username' => $username,
        'password' => $hash
    ));

    header('Location: http://localhost/php-crash-course/form.php');
?>